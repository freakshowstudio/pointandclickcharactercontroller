# README #

A small example project showing a point-and-click type character controller using a Nav Mesh Agent and Mecanim animation with root motion for movement.

Try the WebGL build at this link:

http://static.freakshowstudio.com/unity/pointandclickcharacter/
