﻿
/**
 * @author Stig Olavsen <stig.olavsen@gmail.com>
 * @copyright (C) 2017 - Freakshow Studio AS
 */


using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;


namespace FreakLib.PointAndClickCharacter
{
    [ExecuteInEditMode]
    public class InputBehaviour : MonoBehaviour
    {
        #region Event Classes
        [Serializable]
        public class InputLookEvent : UnityEvent<Vector3> { };

        [Serializable]
        public class InputMoveEvent : UnityEvent<Vector3> { };

        [Serializable]
        public class InputCameraRotateEvent : UnityEvent<Vector3> { };

        [Serializable]
        public class InputCameraZoomEvent : UnityEvent<float> { };
        #endregion // Event Classes


        #region Enums
        private enum InputState
        {
            Default,
            MouseClick,
            MouseRotateCamera,
            KeyRotateCamera,
            MouseZoomCamera,
            KeyZoomCamera,
        }
        #endregion // Enums


        #region Private Variables
        private Vector3     _lastMousePosition  = Vector3.zero;
        private Vector3     _mouseDelta         = Vector3.zero;
        private InputState  _inputState         = InputState.Default;
        private KeyCode     _rotateKeyCode;
        private KeyCode     _zoomKeyCode;
        #endregion  //Private Variables


        #region Events
        [SerializeField]
        private InputLookEvent _lookEvent =
            new InputLookEvent();

        [SerializeField]
        private InputMoveEvent _moveEvent =
            new InputMoveEvent();

        [SerializeField]
        private InputCameraRotateEvent _cameraRotateEvent =
            new InputCameraRotateEvent();

        [SerializeField]
        private InputCameraZoomEvent _cameraZoomEvent =
            new InputCameraZoomEvent();
        #endregion // Events


        #region Event Properties
        public InputLookEvent LookEvent
        {
            get { return _lookEvent; }
        }

        public InputMoveEvent MoveEvent
        {
            get { return _moveEvent; }
        }

        public InputCameraRotateEvent CameraRotateEvent
        {
            get { return _cameraRotateEvent; }
        }

        public InputCameraZoomEvent CameraZoomEvent
        {
            get { return _cameraZoomEvent; }
        }
        #endregion // Event Properties


        #region Unity Methods
        void Update()
        {
            UpdateMouseInput();
            HandleInput();
        }
        #endregion // Unity Methods


        #region Private Methods
        private void UpdateMouseInput()
        {
            Vector3 newMousePosition = Input.mousePosition;
            _mouseDelta = newMousePosition - _lastMousePosition;
            _lastMousePosition = newMousePosition;
        }

        private void HandleInput()
        {
            switch (_inputState)
            {
                case InputState.MouseClick:
                    HandleInputMouseClick();
                    break;
                case InputState.KeyRotateCamera:
                    HandleInputKeyRotateCamera();
                    break;
                case InputState.MouseRotateCamera:
                    HandleInputMouseRotateCamera();
                    break;
                case InputState.KeyZoomCamera:
                    HandleInputKeyZoomCamera();
                    break;
                case InputState.MouseZoomCamera:
                    HandleInputMouseZoomCamera();
                    break;
                case InputState.Default:
                default:
                    HandleInputDefault();
                    break;
            }
        }

        private void HandleInputDefault()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _inputState = InputState.MouseClick;
                return;
            }

            if (Input.GetKeyDown(KeyCode.LeftAlt))
            {
                _rotateKeyCode = KeyCode.LeftAlt;
                _inputState = InputState.KeyRotateCamera;
                return;
            }
            if (Input.GetKeyDown(KeyCode.RightAlt))
            {
                _rotateKeyCode = KeyCode.RightAlt;
                _inputState = InputState.KeyRotateCamera;
                return;
            }
            if (Input.GetKeyDown(KeyCode.AltGr))
            {
                _rotateKeyCode = KeyCode.AltGr;
                _inputState = InputState.KeyRotateCamera;
                return;
            }

            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                _zoomKeyCode = KeyCode.LeftControl;
                _inputState = InputState.KeyZoomCamera;
                return;
            }
            if (Input.GetKeyDown(KeyCode.RightControl))
            {
                _zoomKeyCode = KeyCode.RightControl;
                _inputState = InputState.KeyZoomCamera;
                return;
            }

            if (Input.GetMouseButtonDown(2))
            {
                _inputState = InputState.MouseRotateCamera;
                return;
            }

            float scroll = Input.GetAxis("Mouse ScrollWheel");
            if (scroll != 0f)
            {
                _cameraZoomEvent.Invoke(scroll);
            }

            _lookEvent.Invoke(Input.mousePosition);
        }

        private void HandleInputMouseClick()
        {
            if (Input.GetMouseButtonUp(0))
            {
                _moveEvent.Invoke(Input.mousePosition);
                _inputState = InputState.Default;
                return;
            }
        }

        private void HandleInputKeyRotateCamera()
        {
            if (Input.GetKeyUp(_rotateKeyCode))
            {
                _inputState = InputState.Default;
                return;
            }

            _cameraRotateEvent.Invoke(_mouseDelta);
        }

        private void HandleInputMouseRotateCamera()
        {
            if (Input.GetMouseButtonUp(2))
            {
                _inputState = InputState.Default;
                return;
            }

            _cameraRotateEvent.Invoke(_mouseDelta);
        }

        private void HandleInputKeyZoomCamera()
        {
            if (Input.GetKeyUp(_zoomKeyCode))
            {
                _inputState = InputState.Default;
                return;
            }

            _cameraZoomEvent.Invoke(_mouseDelta.y);
        }

        private void HandleInputMouseZoomCamera()
        {
            _inputState = InputState.Default;
            return;
        }
        #endregion //Private Methods
  }
}
