﻿
/**
 * @author Stig Olavsen <stig.olavsen@gmail.com>
 * @copyright (C) 2017 - Freakshow Studio AS
 */


using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;


namespace FreakLib.PointAndClickCharacter
{
    public class CameraBehaviour : MonoBehaviour
    {
        #region Inspector Variables
        [SerializeField] private Transform  _targetTransform;
        [SerializeField] private float      _rotationSpeed      = 1f;
        [SerializeField] private float      _zoomSpeed          = 1f;
        [Range(-89f, 89f)]
        [SerializeField] private float      _minTiltAngle       = -45f;
        [Range(-89f, 89f)]
        [SerializeField] private float      _maxTiltAngle       = 60f;
        [SerializeField] private float      _minZoom            = 2f;
        [SerializeField] private float      _maxZoom            = 20f;
        #endregion // Inspector Variables


        #region Private Variables
        private Transform _transform;
        private Transform _pivotTransform;
        private Transform _boomTransform;
        #endregion // Private Variables


        #region Unity Methods
        void Awake()
        {
            _transform = GetComponent<Transform>();
            _pivotTransform = _transform.GetChild(0);
            _boomTransform = _pivotTransform.GetChild(0);
        }

        void LateUpdate()
        {
            FollowTarget();
        }
        #endregion // Unity Methods


        #region Public Event Handlers
        public void OnRotate(Vector3 mouseDelta)
        {
            Vector3 amount = mouseDelta * _rotationSpeed;

            float eulerX = Mathf.Clamp(
                _pivotTransform.localEulerAngles.x - amount.y,
                _minTiltAngle,
                _maxTiltAngle);

            float eulerY = _pivotTransform.localEulerAngles.y + amount.x;
            while (eulerY < -180f) eulerY += 360f;
            while (eulerY > 180f) eulerY -= 360f;

            _pivotTransform.localEulerAngles = new Vector3(eulerX, eulerY, 0f);
        }

        public void OnZoom(float delta)
        {
            float amount = delta * _zoomSpeed;

            float posZ = Mathf.Clamp(
                _boomTransform.localPosition.z + amount,
                -_maxZoom,
                -_minZoom);

            _boomTransform.localPosition = new Vector3(0f, 0f, posZ);
        }
        #endregion // Public Event Handlers


        #region Private Methods
        private void FollowTarget()
        {
            _transform.position = _targetTransform.position;
        }
        #endregion // Private Methods
    }
}
