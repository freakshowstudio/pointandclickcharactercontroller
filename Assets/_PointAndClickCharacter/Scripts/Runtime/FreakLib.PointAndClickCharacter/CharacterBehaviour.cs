﻿
/**
 * @author Stig Olavsen <stig.olavsen@gmail.com>
 * @copyright (C) 2017 - Freakshow Studio AS
 */


using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;


namespace FreakLib.PointAndClickCharacter
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(NavMeshAgent))]
    public class CharacterBehaviour : MonoBehaviour
    {
        #region Inspector Variables
        [SerializeField] private float      _animationSmoothFactor  = 2f;
        [SerializeField] private float      _agentPullFactor        = 1f;
        [SerializeField] private Transform  _head;
        [SerializeField] private float      _lookAtSmoothFactor     = 0.2f;
        [Range(0f, 1f)]
        [SerializeField] private float      _lookAtIKWeight         = 0.5f;
        [Range(0f, 1f)]
        [SerializeField] private float      _bodyIKWeight           = 0.5f;
        [Range(0f, 1f)]
        [SerializeField] private float      _headIKWeight           = 0.5f;
        [Range(0f, 1f)]
        [SerializeField] private float      _eyesIKWeight           = 0.5f;
        [Range(0f, 1f)]
        [SerializeField] private float      _clampIKWeight          = 0.5f;
        #endregion // Inspector Variables


        #region Private Variables
        private Transform       _transform;
        private Animator        _animator;
        private NavMeshAgent    _agent;
        private Vector2         _smoothDelta = Vector2.zero;
        private Vector3         _lookForward = Vector3.forward;
        #endregion // Private Variables


        #region Properties
        public Vector3 LookPosition { get; set; }

        public NavMeshAgent Agent
        {
            get { return _agent; }
        }
        #endregion // Properties


        #region Unity Methods
        void Awake()
        {
            _transform = GetComponent<Transform>();
            _animator = GetComponent<Animator>();
            _agent = GetComponent<NavMeshAgent>();

            _lookForward = _transform.forward;

            SetupAgent();
        }

        void Update()
        {
            AnimateAgent();
        }

        void OnAnimatorMove()
        {
            UpdatePositions();
        }

        void OnAnimatorIK(int layer)
        {
            UpdateHeadIK();
        }
        #endregion // Unity Methods


        #region Private Methods
        private void UpdateHeadIK()
        {
            Vector3 lookDirection = LookPosition - _head.position;

            Vector3 lookForwardTarget = 
                Mathf.Approximately(LookPosition.sqrMagnitude, 0f) ? 
                Vector3.ProjectOnPlane(_transform.forward, _transform.up) :
                Vector3.ProjectOnPlane(lookDirection, _transform.up);
            
            lookForwardTarget = Vector3.Normalize(lookForwardTarget);

            float smooth = Time.deltaTime * _lookAtSmoothFactor;

            _lookForward = 
                Vector3.RotateTowards(
                    _lookForward, 
                    lookForwardTarget, 
                    smooth, 
                    float.PositiveInfinity);

            _animator.SetLookAtPosition(_head.position + _lookForward);

            _animator.SetLookAtWeight(
                _lookAtIKWeight, 
                _bodyIKWeight, 
                _headIKWeight, 
                _eyesIKWeight, 
                _clampIKWeight);
        }

        private void UpdatePositions()
        {
            Vector3 animPosition = _animator.rootPosition;
            animPosition.y = _agent.nextPosition.y;

            float agentPull = Time.deltaTime * _agentPullFactor;
            animPosition =
                Vector3.MoveTowards(
                    animPosition, _agent.nextPosition, agentPull);

            _transform.position = animPosition;
            _agent.nextPosition = _transform.position;
        }

        private void AnimateAgent()
        {
            Vector3 worldDelta =
                Vector3.Normalize(_agent.nextPosition - _transform.position);

            float dx = Vector3.Dot(_transform.right, worldDelta);
            float dy = Vector3.Dot(_transform.forward, worldDelta);

            float smoothing = Time.deltaTime * _animationSmoothFactor;
            _smoothDelta = Vector2.MoveTowards(
                _smoothDelta, new Vector2(dx, dy), smoothing);

            bool shouldMove =
                ((dx*dx)+(dy*dy)) > 0.5f &&
                _agent.remainingDistance > _agent.radius;

            _animator.SetBool("Move", shouldMove);
            _animator.SetFloat("Direction", _smoothDelta.x);
            _animator.SetFloat("Speed", _smoothDelta.y);
        }

        private void SetupAgent()
        {
            _agent.updatePosition = false;
        }
        #endregion // Private Methods
    }
}
