﻿
/**
 * @author Stig Olavsen <stig.olavsen@gmail.com>
 * @copyright (C) 2017 - Freakshow Studio AS
 */


using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;


namespace FreakLib.PointAndClickCharacter
{
    public class CursorBehaviour : MonoBehaviour
    {
        #region Inspector Variables
        [SerializeField] private Renderer[] _renderers;
        [SerializeField] private Material   _movementValidMaterial;
        [SerializeField] private Material   _movementInvalidMaterial;
        [SerializeField] private bool       _disableOnInvalid = true;
        #endregion // Inspector Variables


        #region Private Variables
        private Transform _transform;
        #endregion // Private Variables


        #region Properties
        public bool     IsValid     { get; set; }
        public Vector3  Position    { get; set; }
        #endregion // Properties


        #region Unity Methods
        void Awake()
        {
            _transform = GetComponent<Transform>();
        }

        void LateUpdate()
        {
            UpdatePosition();
            UpdateMaterial();
        }
        #endregion // Unity Methods


        #region Private Methods
        private void UpdatePosition()
        {
            _transform.position = Position;
        }

        private void UpdateMaterial()
        {
            Material material =
                IsValid ? _movementValidMaterial : _movementInvalidMaterial;

            for (int i = 0; i < _renderers.Length; ++i)
            {
                _renderers[i].material = material;
                if (_disableOnInvalid)
                {
                    _renderers[i].enabled = IsValid;
                }
            }
        }
        #endregion // Private Methods
    }
}
