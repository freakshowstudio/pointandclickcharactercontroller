﻿
/**
 * @author Stig Olavsen <stig.olavsen@gmail.com>
 * @copyright (C) 2017 - Freakshow Studio AS
 */


using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;


namespace FreakLib.PointAndClickCharacter
{
    [RequireComponent(typeof(CharacterBehaviour))]
    public class PlayerBehaviour : MonoBehaviour
    {
        #region Inspector Variables
        [SerializeField] private Camera             _mainCamera;
        [SerializeField] private CursorBehaviour    _cursor;
        [SerializeField] private LayerMask          _navigatableLayers;
        #endregion // Inspector Variables


        #region Private Variables
        private CharacterBehaviour  _character;
        private NavMeshPath         _currentPath;
        #endregion // Private Variables


        #region Unity Methods
        void Awake()
        {
            _character = GetComponent<CharacterBehaviour>();
            if (_mainCamera == null)
            {
                _mainCamera = Camera.main;
            }
        }

        void Start()
        {
            _currentPath = new NavMeshPath();
        }
        #endregion // Unity Methods


        #region Public Event Handlers
        public void OnLook(Vector3 position)
        {
            Vector3 worldPosition = ScreenToWorldPosition(position);
            _character.LookPosition = worldPosition;
            UpdateCursor(worldPosition);
        }

        public void OnMove(Vector3 position)
        {
            Vector3 worldPosition = ScreenToWorldPosition(position);
            if (CanMove(worldPosition))
            {
                _character.Agent.SetPath(_currentPath);
            }
        }
        #endregion // Public Event Handlers


        #region Private Methods
        private Vector3 ScreenToWorldPosition(Vector3 screenPosition)
        {
            Ray ray = _mainCamera.ScreenPointToRay(screenPosition);
            RaycastHit hit;
            if (Physics.Raycast(
                ray, out hit, _mainCamera.farClipPlane, _navigatableLayers))
            {
                return hit.point;
            }

            Plane plane = new Plane(Vector3.up, Vector3.zero);
            float enter;
            if (plane.Raycast(ray, out enter))
            {
                Vector3 position = ray.origin + (ray.direction * enter);
                return position;
            }

            return Vector3.zero;
        }

        private void UpdateCursor(Vector3 position)
        {
            _cursor.Position = position;
            _cursor.IsValid = CanMove(position);
        }

        private bool CanMove(Vector3 position)
        {
            NavMeshHit navHit;
            bool isNavigatable =
                NavMesh.SamplePosition(
                    position, out navHit, 0.5f, NavMesh.AllAreas);

            if (!isNavigatable)
            {
                return false;
            }

            bool canPath =
                _character.Agent.CalculatePath(navHit.position, _currentPath);
            bool pathComplete =
                _currentPath.status == NavMeshPathStatus.PathComplete;

            return canPath && pathComplete;
        }
        #endregion // Private Methods
    }
}
